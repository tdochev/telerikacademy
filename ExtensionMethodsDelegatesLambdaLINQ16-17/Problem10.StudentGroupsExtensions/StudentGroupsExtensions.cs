﻿namespace Problem10.StudentGroupsExtensions
{
    using System;
    using System.Linq;
    using Models;

    public class StudentGroupsExtensions
    {
        public static void Main(string[] args)
        {
            var students = TestLists.MyStudentsList;
            var studentsFromGroupTwo = students.Where(s => s.GroupNumber == 2).OrderBy(s => s.FirstName);
            Console.WriteLine("All student from group two (with lambda): " + Environment.NewLine + string.Join(Environment.NewLine, studentsFromGroupTwo));
        }
    }
}
