﻿namespace Models
{
    using System.Collections.Generic;

    public class Student
    {
        private List<int> marks;

        public string FirstName
        {
            get;
            private set;
        }

        public string LastName
        {
            get;
            private set;
        }

        public uint Age
        {
            get;
            private set;
        }

        public int FN
        {
            get;
            private set;
        }

        public string Tel
        {
            get;
            private set;
        }

        public string Email
        {
            get;
            private set;
        }

        public List<int> Marks
        {
            get { return new List<int>(marks); }
            set { this.marks = value; }
        }

        public int GroupNumber
        {
            get;
            private set;
        }

        public Student(string firstName, string lastName)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.marks = new List<int>();
        }

        public Student(string firstName, string lastName, uint age)
        : this(firstName, lastName)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Age = age;
        }

        public Student(string firstName, string lastName, uint age, int fn)
        : this(firstName, lastName, age)
        {
            this.FN = fn;
        }

        public Student(string firstName, string lastName, uint age, int fn, string tel)
        : this(firstName, lastName, age, fn)
        {
            this.Tel = tel;
        }

        public Student(string firstName, string lastName, uint age, int fn, string tel, string email)
        : this(firstName, lastName, age, fn, tel)
        {
            this.Email = email;
        }

        public Student(string firstName, string lastName, uint age, int fn, string tel, string email, List<int> marks)
        : this(firstName, lastName, age, fn, tel, email)
        {
            this.Marks = marks;
        }

        public Student(string firstName, string lastName, uint age, int fn, string tel, string email, List<int> marks, int groupNumber)
        : this(firstName, lastName, age, fn, tel, email, marks)
        {
            this.GroupNumber = groupNumber;
        }

        public Student(string firstName, string lastName, int groupNumber)
        : this(firstName, lastName)
        {
            this.GroupNumber = groupNumber;
        }

        public override string ToString()
        {
            return string.Format("Student: FirstName={0}, LastName={1}, Age={2}, FN={3}, Tel={4}, Email={5}, Marks={6}, GroupNumber={7}", this.FirstName, this.LastName, this.Age, this.FN, this.Tel, this.Email, string.Join(", ", this.Marks), this.GroupNumber);
        }
    }
}