﻿using System.Collections.Generic;
using Models;

namespace Models
{
    public static class TestLists
    {
        public static List<Group> MyGroupsList = new List<Group>
            {
                new Group (1, "Programing"),
                new Group (2, "Physics"),
                new Group (3, "Mathematics")
            };

        public static List<Student> MyStudentsList = new List<Student>
            {
                new Student ("Ivan", "Ivanov", 28, 234506,"+35928701543", "ivan@dir.bg", new List<int>{2, 2}, 3),
                new Student ("Gosho", "Goshev", 18, 345508,"+359886234567", "gosho@gmail.com", new List<int>{2, 4, 2}, 2),
                new Student ("Peshov", "Peshov", 19, 237906,"+359888123456", "pesho@abv.bg", new List<int>{2, 5, 6}, 3),
                new Student ("Stefcho", "Petrov", 42, 234211,"028659907", "stefcho@yahoo.com", new List<int>{2, 3, 5}, 1),
                new Student ("Ivan", "Aprilov", 22, 234512,"+359888123456", "ivan@abv.bg", new List<int>{5, 6, 6}, 2)
            };
    }
}
