﻿namespace Problem05.OrderStudents
{
    using System;
    using System.Linq;
    using Models;

    public class OrderStudents
    {
        public static void Main()
        {
            var students = TestLists.MyStudentsList;

            var sortedStudentsLambda = students.OrderByDescending(x => x.FirstName).ThenByDescending(x => x.LastName).ToList();
            var sortedStudentsLINQ = from student in students
                                     orderby student.FirstName descending
                                     orderby student.LastName descending
                                     select student;

            Console.WriteLine("Sorted with lambda:");
            sortedStudentsLambda.ForEach(Console.WriteLine);
            
            Console.WriteLine("Sorted with LINQ:");
            foreach (var student in sortedStudentsLINQ)
            {
                Console.WriteLine(student);
            }
        }
    }
}
