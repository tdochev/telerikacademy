﻿namespace Problem03.FirstBeforeLast
{
    using System;
    using System.Linq;
    using Models;

    public class FirstBeforeLast
    {
        public static void Main()
        {
            var students = new Student[] 
            {
            new Student("Asparuh", "Georgiev"),
            new Student("Todor", "Ivanov"),
            new Student("Ivan", "Goshov"),
            new Student("Gosho", "Ivanov"),
            new Student("Abe", "Ivanov"),
            new Student("Gosho", "Ivanov")
            };

            var studentFirstBeforeLast = FirstNameBeforeLastName(students);
            
            foreach (var student in studentFirstBeforeLast)
            {
            Console.WriteLine(student);
            }
        }

        public static Student[] FirstNameBeforeLastName(Student[] students)
        {
            var result = from student in students
                         where (string.Compare(student.FirstName, student.LastName) < 0)
                         select student;

            return result.ToArray();
        }
    }
}
