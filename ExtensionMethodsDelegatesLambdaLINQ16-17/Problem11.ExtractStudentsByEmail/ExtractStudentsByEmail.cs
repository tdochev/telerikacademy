﻿namespace Problem11.ExtractStudentsByEmail
{
    using System;
    using System.Linq;
    using Models;
    
    public class ExtractStudentsByEmail
    {
        public static void Main()
        {
            var students = TestLists.MyStudentsList;
            var studentsMailInAbv = from student in students
                                    where student.Email.Contains("abv.bg")
                                    select student;
            Console.WriteLine("Students with mail in abv.bg: " + Environment.NewLine + string.Join(Environment.NewLine, studentsMailInAbv));
        }
    }
}