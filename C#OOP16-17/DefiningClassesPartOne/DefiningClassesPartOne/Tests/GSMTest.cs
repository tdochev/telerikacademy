﻿namespace DefiningClassesPartOne.Tests
{
    using System;
    using System.Collections.Generic;

    using DefiningClassesPartOne.Models;

    public class GSMTest
    {
        public static void GSMSPrintTest(IEnumerable<GSM> testArr)
        {
            Console.WriteLine("GSM Test");
            DrawLine();
            foreach (var gsm in testArr)
            {
                Console.WriteLine(gsm);
                Console.WriteLine(new string('_', Console.WindowWidth - 1));
            }
        }

        private static void DrawLine()
        {
            Console.WriteLine(new string('-', Console.WindowWidth - 1));
        }
    }
}
