﻿namespace DefiningClassesPartOne.Tests
{
    using System;
    using System.Linq;
    using DefiningClassesPartOne.Models;

    public static class GSMCallHistoryTest
    {
        public static void GSMPrintCallHistory(GSM gsm)
        {
            Console.WriteLine("Call history test");
            DrawLine('-');
            
            PrintCallHistory(gsm);
            Console.WriteLine("Total calls price: {0:f2}", gsm.GetCallsPrice(0.37m));
            Call logestCall = gsm.CallHistory.OrderByDescending(c => c.Duration).FirstOrDefault();
            gsm.DeleteCall(logestCall);
            Console.WriteLine("Total calls price after deleting the longest call: {0:f2}", gsm.GetCallsPrice(0.37m));
            gsm.ClearCallHistory();
            PrintCallHistory(gsm);
        }

        private static void PrintCallHistory(GSM gsm)
        {
            foreach (var call in gsm.CallHistory)
            {
                Console.WriteLine(call);
                DrawLine('*');
            }
            
            if (gsm.CallHistory.Count == 0)
            {
                Console.WriteLine("The call history is empty!");
            }
        }

        private static void DrawLine(char ch)
        {
            Console.WriteLine(new string(ch, Console.WindowWidth - 1));
        }
    }
}
