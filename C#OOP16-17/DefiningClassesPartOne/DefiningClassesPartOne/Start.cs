﻿namespace DefiningClassesPartOne
{
    using System;
    using Models;
    using Tests;

    public class Start
    {
        public static void Main()
        {
            GSM[] testArray = new GSM[4];
            testArray[0] = new GSM("iPhone 5", "Apple", 1987m, "Gosho", new Battery("Foxconn", 800, 400), new Display(5.3m, 134000));
            testArray[1] = new GSM("G4", "LG", 987m, "Pesho", new Battery("LG Battery", BatteryType.Li_Ion, 800, 400));
            testArray[2] = new GSM("iPhone 4s", "Apple", 687m, "Ivan", new Battery("Foxconn", 800, 400), new Display(5.3m, 134000));

            var batmanGsm = new GSM("iPhone 18s", "Apple", 1800m, "Batman");
            Random rndCallLengthGen = new Random();
            Random rndNumberGen = new Random();
            for (int i = 0; i < 10; i++)
            {
                string number = "+359" + rndNumberGen.Next(82000001, 90000000);
                uint callDuration = (uint)rndCallLengthGen.Next(8, 361);
                var call = new Call(callDuration, number);
                batmanGsm.AddCall(call);
            }

            GSMTest.GSMSPrintTest(testArray);
            GSMCallHistoryTest.GSMPrintCallHistory(batmanGsm);
        }
    }
}
